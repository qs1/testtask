from django.core.validators import RegexValidator
from django.db import models

# Create your models here.
from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager, PermissionsMixin, Group)

from django.db import models
from rest_framework_simplejwt.tokens import RefreshToken


class UserManager(BaseUserManager):

    def create_user(self, username=None, email=None,phone=None, password=None):
        if username is not None:
            user = self.model(username=username)
            # raise TypeError('Users should have a username')
        elif email is not None:
            # username = email.split("@")[0]
            user = self.model(email=self.normalize_email(email))
            # raise TypeError('Users should have a Email')
        elif phone is not None:
            user = self.model(phone=phone)
        else:
            raise TypeError('Users should have a Email or username')

        user.set_password(password)
        user.save()
        my_group = Group.objects.get(name='Normal')
        my_group.user_set.add(user)
        return user

    def create_superuser(self, username, email, password=None):
        if password is None:
            raise TypeError('Password should not be none')

        user = self.create_user(username, email, password)
        user.is_superuser = True
        user.is_staff = True
        user.save()
        return user


AUTH_PROVIDERS = {'facebook': 'facebook', 'google': 'google',
                  'twitter': 'twitter', 'email': 'email', 'username': 'username'}


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=150, unique=True, blank=True, null=True, db_index=True)
    email = models.EmailField(max_length=255, blank=True, null=True, unique=True, db_index=True)
    phone = models.CharField(max_length=13, blank=True, unique=True, null=True)
    device_id = models.IntegerField(blank=True, unique=True, null=True)
    is_verified = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    auth_provider = models.CharField(
        max_length=255, blank=False,
        null=False, default=AUTH_PROVIDERS.get('email'))

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    objects = UserManager()

    def __str__(self):
        if self.username is not None:
            return str(self.username)
        elif self.email is not None:
            return str(self.email)
        elif self.phone is not None:
            return str(self.phone)
        elif self.device_id is not None:
            return str(self.device_id)



    def tokens(self):
        refresh = RefreshToken.for_user(self)
        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token)
        }


class PhoneOTP(models.Model):
    phone_regex = RegexValidator(regex=r'^09(1[0-9]|3[1-9]|2[1-9])-?[0-9]{3}-?[0-9]{4}$',
                                 message="Phone number must be entered in the right format")
    phone = models.CharField(validators=[phone_regex], max_length=17, unique=True)
    otp = models.CharField(max_length=9, blank=True, null=True)
    count = models.IntegerField(default=0, help_text='Number of otp sent')
    logged = models.BooleanField(default=False, help_text='If otp verification got successful')
    forgot = models.BooleanField(default=False, help_text='only true for forgot password')
    forgot_logged = models.BooleanField(default=False, help_text='Only true if validate otp forgot get successful')

    def __str__(self):
        return str(self.phone) + ' is sent ' + str(self.otp)
