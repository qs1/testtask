import jwt
from django.contrib.auth.models import Group
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST, require_http_methods
from django.db.utils import IntegrityError
from rest_framework import generics, status, views, permissions
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from ..authorization import *
from ..serializers import *
from rest_framework.response import Response
from rest_framework.views import APIView
from ..models import *
from django.conf import settings
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from ..renderers import UserRenderer
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import smart_str, smart_bytes, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth import authenticate
from django.urls import reverse
from ..utils import Util
from rest_framework.exceptions import AuthenticationFailed
import logging
from django.http import HttpResponsePermanentRedirect, JsonResponse
import os
from kavenegar import *

logger = logging.getLogger("django")


class CustomRedirect(HttpResponsePermanentRedirect):
    allowed_schemes = [os.environ.get('APP_SCHEME'), 'http', 'https']


class RegisterView(generics.GenericAPIView):
    serializer_class = RegisterSerializer
    email_serializer_class = EmailRegisterSerializer
    renderer_classes = (UserRenderer,)

    def post(self, request):
        user = request.data
        logger.info('%RegisterView.post%_class  post method started.')
        if 'email' in user:
            serializer = self.email_serializer_class(data=user)
        else:
            serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        user_data = serializer.data
        logger.info('%RegisterView.post% new user created. user date: ' + str(user_data))
        current_site = get_current_site(request).domain
        if 'email' in user_data:
            self.send_email(current_site, user_data)
            logger.info('%RegisterView.post% verify email sent to : ' + str(user_data['email']))
        logger.info('%RegisterView.post% successfully finished.')
        return Response(user_data, status=status.HTTP_201_CREATED)

    def send_email(self, current_site, user_data):
        user = User.objects.get(email=user_data['email'])
        token = RefreshToken.for_user(user).access_token
        relativeLink = reverse('email-verify')
        absurl = 'http://' + current_site + relativeLink + "?token=" + str(token)
        email_body = 'Hi ' + user.email.split("@")[0] + \
                     ' Use the link below to verify your email \n' + absurl
        data = {'email_body': email_body, 'to_email': user.email,
                'email_subject': 'Verify your email'}
        Util.send_email(data)
        return Response(user_data, status=status.HTTP_200_OK)


class VerifyEmail(views.APIView):
    serializer_class = EmailVerificationSerializer

    token_param_config = openapi.Parameter(
        'token', in_=openapi.IN_QUERY, description='Description', type=openapi.TYPE_STRING)

    @swagger_auto_schema(manual_parameters=[token_param_config])
    def get(self, request):
        token = request.GET.get('token')
        try:
            payload = jwt.decode(token, settings.SECRET_KEY)
            user = User.objects.get(id=payload['user_id'])
            logger.info('%VerifyEmail.get% method started')
            if not user.is_verified:
                user.is_verified = True
                user.save()
            logger.info('%VerifyEmail.get% Successfully activated')
            return Response({'email': 'ایمیل با موفقیت فعال شد.'}, status=status.HTTP_200_OK)
        except jwt.ExpiredSignatureError as identifier:
            logger.error('%VerifyEmail.get% exception: ' + str(identifier))
            return Response({'error': 'فعالساز منقضی شده است.'}, status=status.HTTP_400_BAD_REQUEST)
        except jwt.exceptions.DecodeError as identifier:
            logger.error('%VerifyEmail.get% exception: ' + str(identifier))
            return Response({'error': 'توکن نامعتبر است.'}, status=status.HTTP_400_BAD_REQUEST)


class LoginAPIView(generics.GenericAPIView):
    username_serializer_class = UsernameLoginSerializer
    serializer_class = LoginSerializer

    def post(self, request):
        if 'email' in request.data:
            serializer = self.serializer_class(data=request.data)
        elif 'username' in request.data:
            serializer = self.username_serializer_class(data=request.data)
        else:
            return Response({'error': 'Username or Email required'}, status=status.HTTP_400_BAD_REQUEST)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class RequestPasswordResetEmail(generics.GenericAPIView):
    serializer_class = ResetPasswordEmailRequestSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)

        email = request.data.get('email', '')

        if User.objects.filter(email=email).exists():
            user = User.objects.get(email=email)
            uidb64 = urlsafe_base64_encode(smart_bytes(user.id))
            token = PasswordResetTokenGenerator().make_token(user)
            current_site = get_current_site(
                request=request).domain
            relativeLink = reverse(
                'password-reset-confirm', kwargs={'uidb64': uidb64, 'token': token})
            # redirect_url : redirect to page to enter new password
            redirect_url = request.data.get('redirect_url', '')
            absurl = 'http://' + current_site + relativeLink
            email_body = 'Hello, \n Use link below to reset your password  \n' + \
                         absurl + "?redirect_url=" + redirect_url
            data = {'email_body': email_body, 'to_email': user.email,
                    'email_subject': 'Reset your passsword'}
            Util.send_email(data)
        return Response({'success': 'We have sent you a link to reset your password'}, status=status.HTTP_200_OK)


@csrf_exempt
@require_http_methods(["POST"])
@permission_classes((IsAuthenticated,))
def update_user_email(request):
    logger.info('$update_user_email$ method started.')
    # Authorization must send in request header
    user = check_authorization(request)
    logger.info('$update_user_email$ request.user: ' + str(user))
    if user:
        # user should sent 'email' field in request POST
        var = json.loads(request.body.decode('utf-8'))
        if var['email']:
            try:
                email = var['email']
                logger.info('$update_user_email$ request input email: ' + str(email))
                instance_user = user
                # new email address set
                instance_user.email = email
                instance_user.is_verified = False
                instance_user.save(update_fields=['email', 'is_verified'], force_update=True)
                logger.info('$update_user_email$ set new email for user: ' + str(instance_user))
                user_data = {
                    "email": instance_user.email
                }

                current_site = get_current_site(request).domain
                # verify email send to new email
                RegisterView().send_email(current_site, user_data)
                logger.info('$update_user_email$ verify email sent to email: ' + str(user_data))
                logger.info('$update_user_email$ method successfully finished')
                return JsonResponse({"message": "ایمیل جدید باموفقیت ثبت شد. لطفا ایمیل خود را فعال کنید."},
                                    status=status.HTTP_200_OK)
            except ValidationError as ve:
                logger.error('exception: ' + str(ve))
                return JsonResponse({"message": " ایمیل وارد شده معتبر نیست."},
                                    status=status.HTTP_406_NOT_ACCEPTABLE)
            except IntegrityError as ie:
                logger.error('exception' + str(ie))
                return JsonResponse({"message": " ایمیل وارد شده تکراری است."},
                                    status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class PasswordTokenCheckAPI(generics.GenericAPIView):
    serializer_class = SetNewPasswordSerializer

    def get(self, request, uidb64, token):

        redirect_url = request.GET.get('redirect_url')

        try:
            id = smart_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(id=id)

            if not PasswordResetTokenGenerator().check_token(user, token):
                if len(redirect_url) > 3:
                    return CustomRedirect(redirect_url + '?token_valid=False')
                else:
                    return CustomRedirect(os.environ.get('FRONTEND_URL', '') + '?token_valid=False')

            if redirect_url and len(redirect_url) > 3:
                return CustomRedirect(
                    redirect_url + '?token_valid=True&message=Credentials Valid &uidb64=' + uidb64 + '&token=' + token)
            else:
                return CustomRedirect(os.environ.get('FRONTEND_URL', '') + '?token_valid=False')

        except DjangoUnicodeDecodeError as identifier:
            try:
                if not PasswordResetTokenGenerator().check_token(user):
                    return CustomRedirect(redirect_url + '?token_valid=False')

            except UnboundLocalError as e:
                return Response({'error': 'Token is not valid, please request a new one'},
                                status=status.HTTP_400_BAD_REQUEST)


class SetNewPasswordAPIView(generics.GenericAPIView):
    serializer_class = SetNewPasswordSerializer

    def patch(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({'success': True, 'message': 'Password reset success'}, status=status.HTTP_200_OK)


class LogoutAPIView(generics.GenericAPIView):
    serializer_class = LogoutSerializer

    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(status=status.HTTP_204_NO_CONTENT)


class RegisterDeviceId(APIView):
    def post(self, request, *args, **kwargs):
        device_id = request.data.get('deviceId', False)
        if device_id:
            logger.info('%RegisterDeviceId%_class  post method started. input :device_id ' + str(device_id))
            device_id = str(device_id)
            user = User.objects.filter(device_id__iexact=device_id)
            if user.exists():
                logger.error('%RegisterDeviceId.post% the device_id is exists in Users ')
                return Response({'status': False,
                                 'detail': 'کاربری وجود دارد. '})
            else:
                user = User.objects.create(device_id=device_id)
                user.save()
                logger.info(
                    '%RegisterDeviceId.post% user has been created successfully. with device_id ' + str(device_id))
                my_group = Group.objects.get(name='Guest')
                my_group.user_set.add(user)
                user.save()
                logger.info('%RegisterDeviceId.post% successfully finished')
                return Response({
                    'status': True,
                    'detail': 'حساب کاربری با موفقیت ایجاد شد.'
                })

        else:
            logger.error('%RegisterDeviceId.post% deviceId" was not received in Post request ')
            return Response({
                'status': 'False',
                'detail': 'لطفا شناسه دستگاه را وارد کنید.'
            })


class LoginGuestAPI(generics.GenericAPIView):

    def post(self, request):
        device_id = request.data.get('deviceId', False)
        if device_id:
            logger.info('%LoginGuestAPI.post%_class  post method started. input :device_id ' + str(device_id))
            if User.objects.filter(device_id=str(device_id)).exists():
                device_id = str(device_id)
                user = User.objects.get(device_id=device_id)
                if user.groups.all()[0].name != 'Guest':
                    logger.error("%LoginGuestAPI.post% there is a ''Normal''user with the input deviceId. ")
                    return Response({'message': 'لطفا با نام کاربری، ایمیل یا شماره تلفن وارد سیستم شوید.'},
                                    status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
                if user.last_login is None:
                    user.first_login = True
                    user.save()

                elif user.first_login:
                    user.first_login = False
                    user.save()

                data = {
                    'device_id': user.device_id,
                    'token': user.tokens()
                }
                logger.info("%LoginGuestAPI.post% method successfully finished. ")
                return Response(data, status=status.HTTP_200_OK)

        else:
            logger.error('%LoginGuestAPI.post% "deviceId" was not received in Post request ')
            return Response({
                'status': 'False',
                'detail': 'لطفا شناسه دستگاه را وارد کنید.'
            })


@csrf_exempt
@require_http_methods(["POST"])
@permission_classes((IsAuthenticated,))
def add_guest_email(request):
    user = check_authorization(request)
    if user:
        logger.info('$add_guest_email$ method started. request.user:  ' + str(user))
        old_group = Group.objects.get(name='Guest')
        new_group = Group.objects.get(name='Normal')
        var = json.loads(request.body.decode('utf-8'))
        if var['email'] and var['password']:
            try:
                email = var['email']
                password = var['password']
                logger.info('$add_guest_email$ inputs=> email: ' + str(email))
                instance_user = user
                instance_user.email = email
                instance_user.set_password(password)
                instance_user.is_verified = False
                instance_user.save(update_fields=['email', 'password', 'is_verified'], force_update=True)
                logger.info('$add_guest_email$ add email to guest user: ' + str(instance_user))
                if instance_user.groups.all()[0].name == 'Guest':
                    old_group.user_set.remove(instance_user)
                    new_group.user_set.add(instance_user)
                    instance_user.save()
                user_data = {
                    "email": instance_user.email
                }
                current_site = get_current_site(request).domain
                RegisterView().send_email(current_site, user_data)
                logger.info('$add_guest_email$ verify email sent to given email address ')
                logger.info('$add_guest_email$ method successfully finished. ')
                return JsonResponse({"message": "ایمیل مورد نظر باموفقیت ثبت شد. لطفا ایمیل خودرا فعال کنید."},
                                    status=status.HTTP_200_OK)
            except ValidationError as ve:
                logger.error('$add_guest_email$ exception message: ' + str(ve))
                return JsonResponse({"message": "ایمیل نامعتبر است."},
                                    status=status.HTTP_406_NOT_ACCEPTABLE)
            except IntegrityError as ie:
                logger.error('$add_guest_email$ exception message: ' + str(ie))
                return JsonResponse({"message": "آدرس ایمیل تکراری است."},
                                    status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            logger.error('$add_guest_email$ Either password or email was not received in Post request')
            return Response({
                'status': 'False',
                'detail': 'ایمیل وپسورد را وارد کنید.'
            })
