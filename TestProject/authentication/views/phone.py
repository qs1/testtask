from random import randint

from django.http import JsonResponse
from rest_framework.views import APIView
from ..serializers import *
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.db.utils import IntegrityError
import json
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from ..authorization import *
from django.core.exceptions import ValidationError as VError
from .views import *
from rest_framework import generics, status, permissions


def send_otp(phone):
    """
    This is an helper function to send otp to session stored phones or
    passed phone number as argument.
    """

    if phone:
        key = randint(100000, 999999)
        phone = str(phone)
        otp_key = str(key)
        return otp_key
    else:
        return False


class ValidatePhoneSendOTP(APIView):
    '''
    This class view takes phone number and if it doesn't exists already then it sends otp
     for first coming phone numbers'''

    def post(self, request, *args, **kwargs):
        phone_number = request.data.get('phone')
        if phone_number:
            logger.info('$ValidatePhoneSendOTP.post$ method started. input=> phone: ' + str(phone_number))
            phone = str(phone_number)
            try:
                PhoneOTP.phone_regex(phone)
                user = User.objects.filter(phone__iexact=phone)
                if user.exists():
                    logger.error('$ValidatePhoneSendOTP.post$ Phone Number already exists')
                    return Response({'status': False, 'detail': 'کاربری با این شماره تلفن از قبل وجود دارد.'})
                    # logic to send the otp and store the phone number and that otp in table.
                else:
                    otp = send_otp(phone)
                    logger.info('$ValidatePhoneSendOTP.post$ OTP code sent')
                    if otp:
                        otp = str(otp)
                        count = 0
                        old = PhoneOTP.objects.filter(phone__iexact=phone)
                        if old.exists():
                            logger.info('$ValidatePhoneSendOTP.post$ OTP code updated for phone: ' + str(phone))
                            old_record = PhoneOTP.objects.get(phone=phone)
                            count = old_record.count
                            old_record.otp = otp
                            old_record.count = count + 1
                            old_record.save()
                        else:
                            logger.info('$ValidatePhoneSendOTP.post$ OTP code insert in db for phone: ' + str(phone))
                            count = count + 1
                            PhoneOTP.objects.create(
                                phone=phone,
                                otp=otp,
                                count=count,
                                logged=False, forgot=False, forgot_logged=False
                            )
                        if count > 7:
                            logger.error('$ValidatePhoneSendOTP.post$ Maximum otp limits reached.')
                            return Response({
                                'status': False,
                                'detail': 'تعداد دفعات مجاز برای دریافت کد به اتمام رسید. با شماره دیگری امتحان کنید.'
                            })

                    else:
                        logger.error('$ValidatePhoneSendOTP.post$ OTP sending error.')
                        return Response({
                            'status': 'False',
                            'detail': "ارسال کد فعالسازی با مشکل مواجه شده است. پس از چند دقیقه مجددا امتحان کنید."
                        })

                    logger.info('$ValidatePhoneSendOTP.post$ Otp has been sent successfully.')
                    return Response({
                        'status': True, 'detail': 'Otp has been sent successfully.otp:' + str(otp)
                    })
            except VError as ve:
                logger.exception('$ValidatePhoneSendOTP.post$ exception: ' + str(ve))
                return Response({
                    'status': False, 'detail': 'شماره تلفن همراه وارد شده صحیح نمی باشد.'
                })

        else:
            logger.exception(
                "$ValidatePhoneSendOTP.post$ I haven't received any phone number. Please do a POST request.")
            return Response({
                'status': 'False', 'detail': "شماره تلفن همراه را وارد کنید."
            })


class ValidateOTP(APIView):
    '''
    If you have received otp, post a request with phone and that otp and you will be redirected to set the password

    '''

    def post(self, request, *args, **kwargs):
        phone = request.data.get('phone', False)
        otp_sent = request.data.get('otp', False)
        logger.info('ValidateOTP.post$ started.input phone: ' + str(phone))
        if phone and otp_sent:
            old = PhoneOTP.objects.filter(phone__iexact=phone)
            if old.exists():
                old = old.first()
                otp = old.otp
                if str(otp) == str(otp_sent):
                    old.logged = True
                    old.save()

                    logger.info('ValidateOTP.post$ OTP matched' )
                    return Response({
                        'status': True,
                        'detail': 'کد معتبر است.لطفا پسوردانتخاب کنید.'
                    })
                else:
                    logger.error('ValidateOTP.post$ OTP incorrect')
                    return Response({
                        'status': False,
                        'detail': 'کد معتبر نیست.دوباره سعی کنید.'
                    })
            else:
                logger.error('ValidateOTP.post$ Phone not recognised in db.')
                return Response({
                    'status': False,
                    'detail': 'ابتدا درخواست کد اعتبارسنجی دهید.'
                })


        else:
            logger.error('ValidateOTP.post$ Either phone or otp was not received in Post request.')
            return Response({
                'status':  False,
                'detail': 'شماره تلفن و کد ارسال شده را وارد کنید.'
            })


class RegisterPhone(APIView):
    '''Takes phone and a password and creates a new user only if otp was verified and phone is new'''

    def post(self, request, *args, **kwargs):
        phone = request.data.get('phone', False)
        password = request.data.get('password', False)
        logger.info('RegisterPhone.post$  started. input phone: '+str(phone))
        if phone and password:
            phone = str(phone)
            user = User.objects.filter(phone__iexact=phone)
            if user.exists():
                logger.error('RegisterPhone.post$ Phone Number already have account account.')
                return Response({'status': False,
                                 'detail': 'کاربری با این شماره تلفن از قبل وجود دارد.'})
            else:
                old = PhoneOTP.objects.filter(phone__iexact=phone)
                if old.exists():
                    old = old.first()
                    if old.logged:
                        temp_data = {'phone': phone, 'password': password}
                        logger.info('RegisterPhone.post$ CreateUserPhoneSerializer.input data.phone: '+str(phone))
                        serializer = CreateUserPhoneSerializer(data=temp_data)
                        logger.info('RegisterPhone.post$ CreateUserPhoneSerializer.output: ' + str(serializer))
                        serializer.is_valid(raise_exception=True)
                        user = serializer.save()
                        user.save()
                        old.delete()
                        logger.info('RegisterPhone.post$ user has been created successfully. ')
                        return Response({
                            'status': True,
                            'detail': 'ایجاد حساب کاربری با موفقیت انجام شد. '
                        })

                    else:
                        logger.error('RegisterPhone.post$  otp was not verified earlier. ')
                        return Response({
                            'status': False,
                            'detail': 'ابتدا شماره همراه را فعال کنید.'

                        })
                else:
                    logger.error('RegisterPhone.post$  Phone number not recognised. ')
                    return Response({
                        'status': False,
                        'detail': 'ابتدا درخواست کد اعتبارسنجی دهید.'
                    })
        else:
            logger.error('RegisterPhone.post$ Either phone or password was not received in Post request ')
            return Response({
                'status': False,
                'detail': 'شماره همراه و رمزعبور را وارد کنید.'
            })


class LoginPhoneAPI(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        serializer = LoginUserPhoneSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        logger.info('LoginPhoneAPI.post$ method started')
        if user.last_login is None:
            user.first_login = True
            user.save()

        elif user.first_login:
            user.first_login = False
            user.save()

        data = {
            'phone': user.phone,
            'token': user.tokens()
        }
        logger.info('LoginPhoneAPI.post$ method output data: '+ str(data))
        return Response(data, status=status.HTTP_200_OK)


@csrf_exempt
@require_http_methods(["POST"])
@permission_classes((IsAuthenticated,))
def update_user_phone(request):
    logger.info('$update_user_phone$ method started.')
    user = check_authorization(request)
    logger.info('$update_user_phone$ method request.user: ' + str(user))
    if user:
        var = json.loads(request.body.decode('utf-8'))
        if var['phone']:
            new_phone = var['phone']
            logger.info('$update_user_phone$ request input phone: ' + str(new_phone))
            instance_user = user

            if PhoneOTP.objects.filter(phone=new_phone).exists():
                # if the new phone number exists in PhoneOTP and verified, the user's phone will be update
                phone_otp_instance = PhoneOTP.objects.get(phone=new_phone)
                if phone_otp_instance.logged:
                    instance_user.phone = new_phone
                    instance_user.save()
                    logger.info('$update_user_phone$ verified new phone "' + str(new_phone) + '" set for user: ' + str(
                        instance_user))
                    logger.info('$update_user_phone$ method successfully finished')
                    phone_otp_instance.delete()
                    return JsonResponse({
                        'status': True,
                        'detail': 'شماره همراه کاربر با موفقیت به روزرسانی شد.'
                    })
                else:
                    logger.error('$update_user_phone$ otp was not verified earlier.phone: ' + str(new_phone))
                    return JsonResponse({
                        'status': False,
                        'detail': 'لطفا کدارسال شده را جهت اعتبار سنجی وارد کنید.'

                    })
            else:
                logger.error('$update_user_phone$ phone number DoesNotExists in PhoneOTP.phone: ' + str(new_phone))
                return JsonResponse({
                    'status': False,
                    'detail': ' درخواست کد اعتبار سنجی برای این شماره همراه داده نشده است.'
                })


@csrf_exempt
@require_http_methods(["POST"])
@permission_classes((IsAuthenticated,))
def add_guest_phone(request):
    user = check_authorization(request)
    if user:
        old_group = Group.objects.get(name='Guest')
        new_group = Group.objects.get(name='Normal')
        var = json.loads(request.body.decode('utf-8'))
        if var['phone'] and var['password']:
            phone = var['phone']
            password = var['password']
            logger.info('$add_guest_phone$  method started. input phone: '+str(phone))
            instance_user = user
            if PhoneOTP.objects.filter(phone=phone).exists():
                phone_otp_instance = PhoneOTP.objects.get(phone=phone)
                if phone_otp_instance.logged:
                    instance_user.phone = phone
                    instance_user.set_password(password)
                    instance_user.save()
                    logger.info('$add_guest_phone$ add new phone to user: '+str(instance_user))
                    if instance_user.groups.all()[0].name == 'Guest':
                        old_group.user_set.remove(instance_user)
                        new_group.user_set.add(instance_user)
                        instance_user.save()
                        logger.info('$add_guest_phone$ change user group to Normal. user: '+str(instance_user))
                    logger.info('$add_guest_phone$ Phone of user has been updated successfully.finished')
                    return JsonResponse({
                        'status': True,
                        'detail': 'شماره تلفن با موفقیت اضافه شد.'
                    })
                else:
                    logger.error('$add_guest_phone$ otp was not verified earlier')
                    return JsonResponse({
                        'status': False,
                        'detail':  'ابتدا شماره همراه را فعال کنید.'

                    })
            else:
                logger.error('$add_guest_phone$ Phone number not recognised.')
                return JsonResponse({
                    'status': False,
                    'detail':  'ابتدا درخواست کد اعتبارسنجی دهید.'
                })
