from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from django.urls import path, include

from .views.views import *
from .views.phone import *

urlpatterns = [
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('register/', RegisterView.as_view(), name="register"),
    path('login/', LoginAPIView.as_view(), name="login"),
    path('logout/', LogoutAPIView.as_view(), name="logout"),
    path('email-verify/', VerifyEmail.as_view(), name="email-verify"),
    path('validate_phone/',ValidatePhoneSendOTP.as_view(),name="validate_phone"),
    path('validate_otp/',ValidateOTP.as_view(),name="validate_otp"),
    path('register_phone/',RegisterPhone.as_view(),name="register_phone"),
    path('login_phone/',LoginPhoneAPI.as_view(),name="login_phone"),
    path('request-reset-email/', RequestPasswordResetEmail.as_view(),
         name="request-reset-email"),
    path('password-reset/<uidb64>/<token>/',
         PasswordTokenCheckAPI.as_view(), name='password-reset-confirm'),
    path('password-reset-complete', SetNewPasswordAPIView.as_view(),
         name='password-reset-complete'), # request method should be 'patch'
    path('update_email/',update_user_email,name='update_email'),
    path('update_phone/',update_user_phone,name='update_phone'),
    path('register_deviceId/',RegisterDeviceId.as_view(),name='register_deviceId'),
   path('login_guest/',LoginGuestAPI.as_view(),name='login_guest'),
    path('add_email_guest/',add_guest_email,name='add_email_guest'),
    path('add_phone_guest/',add_guest_phone,name='add_phone_guest'),



]






