from rest_framework.exceptions import ValidationError
from rest_framework_simplejwt.backends import TokenBackend
import requests
from .models import *


def check_authorization(request):
    token = request.META.get('HTTP_AUTHORIZATION', " ").split(' ')[1]
    data = {'token': token}
    try:
        valid_data = TokenBackend(algorithm='HS256').decode(token, verify=False)
        user_id = valid_data['user_id']
        user = User.objects.get(id=user_id)
        return user
    except ValidationError as v:
        print("validation error", v)
